import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class TheaterManagement {
	private long[][] first = new long[15][20];
	private long[][] second = new long[15][20];
	private long[][] third = new long[15][20];
	private String[] stop = {"Sun","Sat"};
	private ArrayList<String> dayStop = new ArrayList(Arrays.asList(stop));
	private String[] work = {"Mon","Tue","Wed","Thu","Fri"};
	private ArrayList<String> dayWork = new ArrayList(Arrays.asList(work));
	
	private Date date = new Date();
	private String day = date.toString().substring(0,3);
	
	//Buy : All time ,  Row , Colum
	public long buyTicket(char cRow, int colum){
		int row = cRow-65;
		if(dayWork.contains(day)){
			long price = DataSeatPrice.ticketPrices[row][colum];
			if(price!=first[row][colum]){
				first[row][colum] = Long.parseLong((""+price));
				return price;
			}
			else if(price!=second[row][colum]){
				second[row][colum] = Long.parseLong((""+price));
				return price;
			}
		}
		
		else if(dayStop.contains(day)){
			long price = DataSeatPrice.ticketPrices[row][colum];
			if(price!=first[row][colum]){
				first[row][colum] = Long.parseLong((""+price));
				return price;
			}
			if(price!=second[row][colum]){
				second[row][colum] = Long.parseLong((""+price));
				return price;
			}
			if(price!=third[row][colum]){
				third[row][colum] = Long.parseLong((""+price));
				return price;
			}
		}
		return 0;
	}
	
	//Buy : Choose a time , Row , Colum
	public long buyTicket(int time, char cRow, int colum){
		int row = cRow-65;
		if(dayWork.contains(day) && time==1){
			long price = DataSeatPrice.ticketPrices[row][colum];
			if(price!=first[row][colum]){
				first[row][colum] = Long.parseLong((""+price));
				return price;
			}
		}
		else if(dayWork.contains(day) && time==2){
			long price = DataSeatPrice.ticketPrices[row][colum];
			if(price!=second[row][colum]){
				second[row][colum] = Long.parseLong((""+price));
				return price;
			}
		}
		else if(dayStop.contains(day) && time==1){
			long price = DataSeatPrice.ticketPrices[row][colum];
			if(price!=first[row][colum]){
				first[row][colum] = Long.parseLong((""+price));
				return price;
			}
		}
		else if(dayStop.contains(day) && time==2){
			long price = DataSeatPrice.ticketPrices[row][colum];
			if(price!=second[row][colum]){
				second[row][colum] = Long.parseLong((""+price));
				return price;
			}
		}
		else if(dayStop.contains(day) && time==3){
			long price = DataSeatPrice.ticketPrices[row][colum];
			if(price!=third[row][colum]){
				third[row][colum] = Long.parseLong((""+price));
				return price;
			}
		}
		return 0;
	}
	
	//Buy : Price
	public void buyTicket(long price){
		
		if(dayWork.contains(day)){
			System.out.println("===========================================================================================================================================================\n\n");
			System.out.println("<------------------------------------------------------------------------TIME : 1------------------------------------------------------------------------>");
			for(int i = 0 ; i < 15 ; i++){
				for(int j = 0 ; j < 20 ; j++){
					long seat = DataSeatPrice.ticketPrices[i][j];
					if(seat==price && seat!=first[i][j]) System.out.print(seat+"\t");
					else System.out.print(0+"\t");
				}
				System.out.println();
			}
			System.out.println("===========================================================================================================================================================\n\n");
			System.out.println("<------------------------------------------------------------------------TIME : 2------------------------------------------------------------------------>");
			for(int i = 0 ; i < 15 ; i++){
				for(int j = 0 ; j < 20 ; j++){
					long seat = DataSeatPrice.ticketPrices[i][j];
					if(seat==price && seat!=second[i][j])System.out.print(seat+"\t");
					else System.out.print(0+"\t");
				}
				System.out.println();
			}
		}
		else if(dayStop.contains(day)){
			System.out.println("===========================================================================================================================================================\n\n");
			System.out.println("<------------------------------------------------------------------------TIME : 1------------------------------------------------------------------------>");
			for(int i = 0 ; i < 15 ; i++){
				for(int j = 0 ; j < 20 ; j++){
					long seat = DataSeatPrice.ticketPrices[i][j];
					if(seat==price && seat!=first[i][j]) System.out.print(seat+"\t");
					else System.out.print(0+"\t");
				}
				System.out.println();
			}
			System.out.println("===========================================================================================================================================================\n\n");
			System.out.println("<------------------------------------------------------------------------TIME : 2------------------------------------------------------------------------>");
			for(int i = 0 ; i < 15 ; i++){
				for(int j = 0 ; j < 20 ; j++){
					long seat = DataSeatPrice.ticketPrices[i][j];
					if(seat==price && seat!=second[i][j])System.out.print(seat+"\t");
					else System.out.print(0+"\t");
				}
				System.out.println();
			}
			System.out.println("===========================================================================================================================================================\n\n");
			System.out.println("<------------------------------------------------------------------------TIME : 3------------------------------------------------------------------------>");
			for(int i = 0 ; i < 15 ; i++){
				for(int j = 0 ; j < 20 ; j++){
					long seat = DataSeatPrice.ticketPrices[i][j];
					if(seat==price && seat!=third[i][j])System.out.print(seat+"\t");
					else System.out.print(0+"\t");
				}
				System.out.println();
			}
		}
	}
}
