import java.util.Date;

public class Main {
	public static void main(String[] args) {
		TheaterManagement theater = new TheaterManagement();
		for(int i = 0 ; i < 15 ; i++){
			for(int j = 0 ; j < 20 ; j++){
				System.out.print(DataSeatPrice.ticketPrices[i][j]+"\t");
			}
			System.out.println();
		}
		System.out.println("===========================================================================================================================================================\n");
		
		//Test Case
		System.out.println(theater.buyTicket(1, 'O', 11));
		System.out.println(theater.buyTicket(1, 'O', 12));
		theater.buyTicket(20);
		theater.buyTicket(50);
		System.out.println(theater.buyTicket('O', 12));
		System.out.println(theater.buyTicket('O', 13));
		
	}
}